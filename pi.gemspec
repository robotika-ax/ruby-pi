# frozen_string_literal: true

require_relative 'lib/pi/version'

Gem::Specification.new do |spec|
  spec.name          = 'pi.rb'
  spec.version       = Pi::VERSION
  spec.authors       = ['Jan Lindblom']
  spec.email         = ['janlindblom@fastmail.fm']

  spec.summary       = 'Ruby I/O library for Raspberry Pi (GPIO, I2C, SPI, UART).'
  spec.homepage      = 'https://bitbucket.org/robotika-ax/ruby-pi'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/robotika-ax/ruby-pi/src/'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/robotika-ax/ruby-pi/src/master/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'pry', '~> 0.13'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.82'
  spec.add_development_dependency 'yard', '~> 0.9'
end
