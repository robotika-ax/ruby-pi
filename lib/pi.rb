# frozen_string_literal: true

require 'pi/version'
require 'pi/platform'
require 'pi/system'

# Pi module.
module Pi
  # A general error.
  class Error < StandardError; end
  # Your code goes here...
end
