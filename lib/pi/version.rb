# frozen_string_literal: true

module Pi
  # Current version of library.
  VERSION = '0.0.1'
end
