# frozen_string_literal: true

module Pi
  module System
    # SystemInfo class
    class SystemInfo
      def self.get_processor; end
    end
  end
end
